#include <iostream>

using namespace std;

int main()
{
    int a[30];
    int i, sum, oldsum;
    for(i = 0; i < 30; i++){cin >> a[i];}
    sum = 0;
    oldsum = 0;
    for(i = 1; i < 30; i++){
        if (a[i] > a[i - 1]){sum++;}
        else if(sum > oldsum){oldsum = sum; sum = 1;}
    }
    if (sum > oldsum){cout << sum;}
    else {cout << oldsum;}
    return 0;
}

