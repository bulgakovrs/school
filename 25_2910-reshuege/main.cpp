#include <iostream>
#define F 3
using namespace std;

int main()
{
    int a[F][F];
    int sum, max, i, k;
    for (i = 0; i < F; i++){
        for (k = 0; k < F; k++){
            cin >> a[i][k];
        }
    }
    for (i = 0; i < F; i++){
        max = a[i][0];
        for (k = 0; k < F; k++){
            if (a[i][k] > max){
                max = a[i][k];
            }
        }
        sum += max;
    }
    cout << "Сумма максимальных элементов строки "
            "двумерного массива = " << sum << endl;
    return 0;
}

