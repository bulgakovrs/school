#include <iostream>
#define N 5
using namespace std;

int main()
{
    int a[N];
    int i, j; //, s; - не пригодилась
    j = 0;
    for (i = 0; i < N; i++){
        cin >> a[i];
    }

    for (i = 0; i < N; i++){
        if (a[i] < 1000 && a[i] > 99 && a[i] % 10 == 9 && a[i] % 100 != 99){
            j+= a[i];
        }
    }
    if (j > 0) {
        cout << "Сумма всех со­дер­жа­щих­ся в мас­си­ве трёхзнач­ных чисел,"
                        " де­ся­тич­ная за­пись ко­то­рых окан­чи­ва­ет­ся на 9,"
                        " но не на 99 = " << j;
    } else {cout << "Не найдено" << endl;}
}

