#include <iostream>
#define N 5
using namespace std;

int main()
{
    int a[N];
    int i, max;
    max = 0;
    for (i = 0; i < N; i++){cin>>a[i];}
    for (i = 0; i < N; i++){
        if (a[i] < 100 && a[i] > 9 && a[i] % 3 != 0 && max < a[i]){
            max = a[i];
        }
    }
    if (max == 0){cout << "Не найдено";}
    else {cout << max;}
    return 0;
}
