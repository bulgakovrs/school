#include <iostream>
#define N 30
using namespace std;

int main()
{
    int a[N];
    int i, min;
    min = 0;
    for (i = 0; i < N; i++) {cin >> a[i];}
    for (i = 0; i < N; i++){
        if (a[i] > 99 && a[i] < 1000 && a[i] % 7 == 0 && a[i] < min) {
            min = a[i];
        }
    }
    if (min == 0){cout << "Не найдено";}
    else {cout << min;}
    return 0;
}

